package fr.hien.football.presentation.extension

import fr.hien.football.domain.model.League
import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class LeaguesNetworkResponseExtTest {

    @ParameterizedTest(name = "WHEN NetworkResponse: {0} THEN toLeaguesNames returns: {1}")
    @MethodSource("provideTestData")
    fun toLeaguesNames(
        networkResponse: NetworkResponse<Leagues>,
        expected: List<String>
    ) {
        assertEquals(expected, networkResponse.toLeaguesNames())
    }

    companion object {

        private val premierLeague = League(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val laLiga = premierLeague.copy(
            name = "La Liga"
        )

        @JvmStatic
        private fun provideTestData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    NetworkResponse<Leagues>(),
                    emptyList<String>()
                ),
                Arguments.of(
                    NetworkResponse(
                        status = RequestStatus.SUCCESS,
                        data = Leagues(
                            leagues = listOf(premierLeague, laLiga)
                        )
                    ),
                    listOf("English Premier League", "La Liga")
                ),
                Arguments.of(
                    NetworkResponse(
                        status = RequestStatus.SUCCESS,
                        data = Leagues(
                            leagues = listOf(laLiga, premierLeague)
                        )
                    ),
                    listOf("English Premier League", "La Liga")
                ),
                Arguments.of(
                    NetworkResponse<Leagues>(
                        status = RequestStatus.SUCCESS
                    ),
                    emptyList<String>()
                ),
                Arguments.of(
                    NetworkResponse<Leagues>(
                        status = RequestStatus.SUCCESS,
                        data = Leagues(leagues = emptyList())
                    ),
                    emptyList<String>()
                )
            )
        }
    }
}