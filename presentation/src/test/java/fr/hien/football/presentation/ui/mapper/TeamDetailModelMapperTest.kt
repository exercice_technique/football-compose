package fr.hien.football.presentation.ui.mapper

import fr.hien.football.domain.model.Team
import fr.hien.football.presentation.R
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.TeamDetailUIModel
import fr.hien.football.presentation.ui.model.TextUIModel
import fr.hien.football.presentation.ui.state.TeamDetailUIState
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class TeamDetailModelMapperTest {

    @InjectMockKs
    private lateinit var mapper: TeamDetailModelMapper

    @Test
    fun toErrorUIState() {
        // WHEN
        val actual = mapper.toErrorUIState(TEAM_NAME)

        // THEN
        assertEquals(errorUIState, actual)
    }

    @Test
    fun toSuccessUIState() {
        // WHEN
        val actual = mapper.toSuccessUIState(team)

        // THEN
        assertEquals(successUIState, actual)
    }

    companion object {

        private const val TEAM_NAME = "PSG"
        private val errorUIState = TeamDetailUIState.Error(
            uiModel = ErrorUIModel(
                text = TextUIModel.StringResource(R.string.error_of_get_team, arrayOf(TEAM_NAME))
            )
        )
        private val team = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val successUIState = TeamDetailUIState.Success(
            uiModel = TeamDetailUIModel(
                name = TextUIModel.DynamicString(team.name),
                bannerUrl = team.bannerUrl.orEmpty(),
                countryName = TextUIModel.DynamicString(team.countryName.orEmpty()),
                leagueName = TextUIModel.DynamicString(team.leagueName.orEmpty()),
                description = TextUIModel.DynamicString(team.englishDescription.orEmpty()),
            )
        )
    }
}