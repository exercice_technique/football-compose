package fr.hien.football.presentation.ui.mapper

import com.nhaarman.mockitokotlin2.given
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.domain.usecase.GetTeamsByLeagueNameUseCase
import fr.hien.football.presentation.R
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.TeamUIModel
import fr.hien.football.presentation.ui.model.TextUIModel
import fr.hien.football.presentation.ui.state.FootballUIState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.quality.Strictness
import java.net.HttpURLConnection
import java.util.stream.Stream

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
@MockitoSettings(strictness = Strictness.LENIENT)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
internal class FootballModelMapperTest {

    @InjectMocks
    private lateinit var mapper: FootballModelMapper

    @Mock
    private lateinit var getTeamsByLeagueNameUseCase: GetTeamsByLeagueNameUseCase

    @Test
    fun toSuccessUIState() {
        // GIVEN
        val allLeagues = listOf("PSG")

        // WHEN
        val actual = mapper.toSuccessUIState(allLeagues)

        // THEN
        val expected = FootballUIState.Success(
            leagues = allLeagues,
            teams = emptyList()
        )
        assertEquals(expected, actual)
    }

    @Test
    fun toErrorUIState() {
        // WHEN
        val actual = mapper.toErrorUIState()

        // THEN
        val expected = FootballUIState.Error(
            uiModel = ErrorUIModel(
                text = TextUIModel.StringResource(R.string.error_of_get_leagues)
            )
        )
        assertEquals(expected, actual)
    }

    @ParameterizedTest(name = "WHEN currentState: {0}, searchText: {1}, allLeagues: {2}, GetTeamsByLeagueNameUseCase gives: {3} THEN updateUIStateBySearchText returns: {4}")
    @MethodSource("provideTestDataForUpdateUIStateBySearchText")
    fun updateUIStateBySearchText(
        currentState: FootballUIState,
        searchText: String,
        allLeagues: List<String>,
        teamsResult: NetworkResponse<Teams>,
        expected: FootballUIState
    ) = runTest {
        // GIVEN
        given(getTeamsByLeagueNameUseCase(searchText)).willReturn(teamsResult)

        // WHEN
        val actual = mapper.updateUIStateBySearchText(
            currentState = currentState,
            searchText = searchText,
            allLeagues = allLeagues
        )

        // THEN
        assertEquals(expected, actual)
    }


    companion object {

        private val emptySuccessState = FootballUIState.Success(
            leagues = emptyList(),
            teams = emptyList()
        )
        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val lilleTeam = psgTeam.copy(
            name = "Lille",
            badgeUrl = "lille-url"
        )
        private val teamsResult = NetworkResponse(
            status = RequestStatus.SUCCESS,
            code = HttpURLConnection.HTTP_OK,
            data = Teams(teams = listOf(lilleTeam, psgTeam))
        )
        private val errorState = FootballUIState.Error(
            uiModel = ErrorUIModel(text = TextUIModel.DynamicString("Error"))
        )
        private val successState = FootballUIState.Success(
            leagues = emptyList(),
            teams = listOf(
                TeamUIModel(badgeUrl = "psg-url", name = "PSG"),
                TeamUIModel(badgeUrl = "lille-url", name = "Lille")
            )
        )

        @JvmStatic
        private fun provideTestDataForUpdateUIStateBySearchText(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    errorState,
                    "League 1",
                    listOf("League 1"),
                    teamsResult,
                    errorState
                ),
                Arguments.of(
                    FootballUIState.Loading,
                    "League 1",
                    listOf("League 1"),
                    teamsResult,
                    FootballUIState.Loading
                ),
                Arguments.of(
                    emptySuccessState,
                    "League 1",
                    listOf("League 2"),
                    teamsResult,
                    successState
                ),
                Arguments.of(
                    emptySuccessState,
                    "League 2",
                    listOf("League 2"),
                    teamsResult,
                    successState.copy(
                        leagues = listOf("League 2")
                    )
                ),
                Arguments.of(
                    successState,
                    "League 1",
                    listOf("League 2"),
                    NetworkResponse<Teams>(status = RequestStatus.SUCCESS),
                    emptySuccessState
                ),
                Arguments.of(
                    successState,
                    "League",
                    listOf("League 1", "League 2", "La Liga"),
                    NetworkResponse<Teams>(status = RequestStatus.SUCCESS),
                    emptySuccessState.copy(
                        leagues = listOf("League 1", "League 2")
                    )
                ),
                Arguments.of(
                    successState,
                    "League 1",
                    listOf("League 2"),
                    NetworkResponse(status = RequestStatus.SUCCESS, data = Teams(emptyList())),
                    emptySuccessState
                )
            )
        }
    }
}