package fr.hien.football.presentation

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import fr.hien.football.domain.model.League
import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.domain.usecase.GetLeaguesUseCase
import fr.hien.football.domain.usecase.GetTeamDetailByNameUseCase
import fr.hien.football.presentation.extension.toLeaguesNames
import fr.hien.football.presentation.ui.mapper.FootballModelMapper
import fr.hien.football.presentation.ui.mapper.TeamDetailModelMapper
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.TeamDetailUIModel
import fr.hien.football.presentation.ui.model.TextUIModel
import fr.hien.football.presentation.ui.state.FootballUIState
import fr.hien.football.presentation.ui.state.TeamDetailUIState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import java.net.HttpURLConnection
import java.util.stream.Stream

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class FootballViewModelTest {

    @InjectMocks
    private lateinit var viewModel: FootballViewModel

    @Mock
    private lateinit var getLeaguesUseCase: GetLeaguesUseCase

    @Mock
    private lateinit var getTeamDetailByNameUseCase: GetTeamDetailByNameUseCase

    @Mock
    private lateinit var footballModelMapper: FootballModelMapper

    @Mock
    private lateinit var teamDetailModelMapper: TeamDetailModelMapper

    @BeforeEach
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @AfterEach
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @ParameterizedTest(name = "WHEN GetLeaguesUseCase gives: {0} THEN initUIState update the uiState to: {1}")
    @MethodSource("provideTestDataForInitUIState")
    fun initUIState(
        leaguesResult: NetworkResponse<Leagues>,
        expected: FootballUIState
    ) = runTest {
        // GIVEN
        given(getLeaguesUseCase()).willReturn(leaguesResult)
        when (expected) {
            is FootballUIState.Success -> given(footballModelMapper.toSuccessUIState(leaguesResult.toLeaguesNames())).willReturn(expected)
            is FootballUIState.Error -> given(footballModelMapper.toErrorUIState()).willReturn(expected)
            else -> Unit
        }

        // WHEN
        viewModel.initUIState()

        // THEN
        then(getLeaguesUseCase).should()()
        assertEquals(expected, viewModel.uiState.value)
    }

    @Test
    fun onSearchChanged() = runTest {
        // GIVEN
        val searchText = "League 1"
        given(
            footballModelMapper.updateUIStateBySearchText(
                currentState = FootballUIState.Loading,
                searchText = searchText,
                allLeagues = emptyList()
            )
        ).willReturn(successState)

        // WHEN
        viewModel.onSearchChanged(searchText)

        // THEN
        then(footballModelMapper).should().updateUIStateBySearchText(
            currentState = FootballUIState.Loading,
            searchText = searchText,
            allLeagues = emptyList()
        )
        assertEquals(successState, viewModel.uiState.value)
    }

    @Test
    fun `WHEN GetTeamDetailByNameUseCase gives null THEN initTeamDetailState updates the ui state to error`() = runTest {
        // GIVEN
        val teamFlow = flowOf<Team?>(null)
        viewModel.selectedTeamName = TEAM_NAME
        given(getTeamDetailByNameUseCase(TEAM_NAME)).willReturn(teamFlow)
        given(teamDetailModelMapper.toErrorUIState(TEAM_NAME)).willReturn(errorUIState)

        // WHEN
        viewModel.initTeamDetailState()

        // THEN
        then(getTeamDetailByNameUseCase).should()(TEAM_NAME)
        then(teamDetailModelMapper).should().toErrorUIState(TEAM_NAME)
        assertEquals(errorUIState, viewModel.teamDetailState.value)
    }

    @Test
    fun `WHEN GetTeamDetailByNameUseCase gives a team THEN initTeamDetailState updates the ui state to success`() = runTest {
        // GIVEN
        val teamFlow = flowOf<Team?>(team)
        viewModel.selectedTeamName = TEAM_NAME
        given(getTeamDetailByNameUseCase(TEAM_NAME)).willReturn(teamFlow)
        given(teamDetailModelMapper.toSuccessUIState(team)).willReturn(teamDetailUIState)

        // WHEN
        viewModel.initTeamDetailState()

        // THEN
        then(getTeamDetailByNameUseCase).should()(TEAM_NAME)
        then(teamDetailModelMapper).should().toSuccessUIState(team)
        assertEquals(teamDetailUIState, viewModel.teamDetailState.value)
    }

    companion object {

        private val successState = FootballUIState.Success(
            leagues = emptyList(),
            teams = emptyList()
        )
        private const val TEAM_NAME = "PSG"
        private val team = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val errorUIState = TeamDetailUIState.Error(
            uiModel = ErrorUIModel(
                text = TextUIModel.StringResource(R.string.error_of_get_team, arrayOf(TEAM_NAME))
            )
        )
        private val teamDetailUIState = TeamDetailUIState.Success(
            uiModel = TeamDetailUIModel(
                name = TextUIModel.DynamicString(team.name),
                bannerUrl = team.bannerUrl.orEmpty(),
                countryName = TextUIModel.DynamicString(team.countryName.orEmpty()),
                leagueName = TextUIModel.DynamicString(team.leagueName),
                description = TextUIModel.DynamicString(team.englishDescription.orEmpty()),
            )
        )
        private val premierLeague = League(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val league1 = premierLeague.copy(
            name = "French League 1"
        )
        private val leagueResult = NetworkResponse(
            status = RequestStatus.SUCCESS,
            code = HttpURLConnection.HTTP_OK,
            data = Leagues(leagues = listOf(league1, premierLeague))
        )
        private val emptySuccessState = FootballUIState.Success(
            leagues = emptyList(),
            teams = emptyList()
        )

        @JvmStatic
        private fun provideTestDataForInitUIState(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    NetworkResponse<Leagues>(),
                    FootballUIState.Error(
                        uiModel = ErrorUIModel(
                            text = TextUIModel.StringResource(R.string.error_of_get_leagues)
                        )
                    )
                ),
                Arguments.of(
                    NetworkResponse<Leagues>(status = RequestStatus.SUCCESS),
                    emptySuccessState
                ),
                Arguments.of(
                    NetworkResponse(
                        status = RequestStatus.SUCCESS,
                        data = Leagues(emptyList())
                    ),
                    emptySuccessState
                ),
                Arguments.of(
                    leagueResult,
                    FootballUIState.Success(
                        leagues = listOf(
                            "English Premier League",
                            "French League 1"
                        ),
                        teams = emptyList()
                    )
                )
            )
        }
    }
}