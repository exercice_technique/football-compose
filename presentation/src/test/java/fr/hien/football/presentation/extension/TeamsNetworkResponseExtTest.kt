package fr.hien.football.presentation.extension

import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import fr.hien.football.presentation.ui.model.TeamUIModel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.net.HttpURLConnection
import java.util.stream.Stream

class TeamsNetworkResponseExtTest {

    @ParameterizedTest(name = "WHEN NetworkResponse: {0} THEN toTeamUIModels returns: {1}")
    @MethodSource("provideTestData")
    fun toLeaguesNames(
        networkResponse: NetworkResponse<Teams>,
        expected: List<TeamUIModel>
    ) {
        assertEquals(expected, networkResponse.toTeamUIModels())
    }

    companion object {

        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val lilleTeam = psgTeam.copy(
            name = "Lille",
            badgeUrl = "lille-url"
        )
        private val teamsNetworkResponse = NetworkResponse(
            status = RequestStatus.SUCCESS,
            code = HttpURLConnection.HTTP_OK,
            data = Teams(teams = listOf(lilleTeam, psgTeam))
        )

        @JvmStatic
        private fun provideTestData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    NetworkResponse<Teams>(),
                    emptyList<TeamUIModel>()
                ),
                Arguments.of(
                    teamsNetworkResponse,
                    listOf(
                        TeamUIModel(
                            name = "PSG",
                            badgeUrl = "psg-url"
                        ),
                        TeamUIModel(
                            name = "Lille",
                            badgeUrl = "lille-url"
                        )
                    )
                ),
                Arguments.of(
                    NetworkResponse<Teams>(
                        status = RequestStatus.SUCCESS
                    ),
                    emptyList<String>()
                ),
                Arguments.of(
                    NetworkResponse(
                        status = RequestStatus.SUCCESS,
                        data = Teams(teams = emptyList())
                    ),
                    emptyList<String>()
                )
            )
        }
    }
}