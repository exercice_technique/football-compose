package fr.hien.football.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.hien.football.data.source.remote.LeaguesWS
import retrofit2.Retrofit

@InstallIn(SingletonComponent::class)
@Module
class WebServiceModule {

    @Provides
    fun provideLeaguesWS(
        retrofit: Retrofit
    ): LeaguesWS = retrofit.create(LeaguesWS::class.java)
}
