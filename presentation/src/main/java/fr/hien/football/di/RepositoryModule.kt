package fr.hien.football.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.hien.football.data.repository.LeaguesRepositoryImpl
import fr.hien.football.data.repository.TeamsRepositoryImpl
import fr.hien.football.domain.repository.LeaguesRepository
import fr.hien.football.domain.repository.TeamsRepository

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideTeamsRepository(
        repository: TeamsRepositoryImpl
    ): TeamsRepository

    @Binds
    abstract fun provideLeaguesRepository(
        repository: LeaguesRepositoryImpl
    ): LeaguesRepository
}
