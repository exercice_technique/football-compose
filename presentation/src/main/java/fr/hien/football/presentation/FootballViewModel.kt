package fr.hien.football.presentation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.domain.usecase.GetLeaguesUseCase
import fr.hien.football.domain.usecase.GetTeamDetailByNameUseCase
import fr.hien.football.presentation.extension.toLeaguesNames
import fr.hien.football.presentation.ui.mapper.FootballModelMapper
import fr.hien.football.presentation.ui.mapper.TeamDetailModelMapper
import fr.hien.football.presentation.ui.state.FootballUIState
import fr.hien.football.presentation.ui.state.TeamDetailUIState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FootballViewModel @Inject constructor(
    private val getLeaguesUseCase: GetLeaguesUseCase,
    private val getTeamDetailByNameUseCase: GetTeamDetailByNameUseCase,
    private val footballModelMapper: FootballModelMapper,
    private val teamDetailModelMapper: TeamDetailModelMapper
) : ViewModel() {

    // Flow UI
    private val _uiState = MutableStateFlow<FootballUIState>(FootballUIState.Loading)
    val uiState: StateFlow<FootballUIState> = _uiState.asStateFlow()

    private val _teamDetailState = MutableStateFlow<TeamDetailUIState>(TeamDetailUIState.Loading)
    val teamDetailState: StateFlow<TeamDetailUIState> = _teamDetailState.asStateFlow()

    var selectedTeamName: String = ""
    var currentSearchText by mutableStateOf("")
    var allLeaguesNames = emptyList<String>()

    fun initUIState() {
        viewModelScope.launch {
            _uiState.update { FootballUIState.Loading }
            val leaguesResult = getLeaguesUseCase()
            allLeaguesNames = leaguesResult.toLeaguesNames()
            _uiState.update {
                when (leaguesResult.status) {
                    RequestStatus.SUCCESS -> footballModelMapper.toSuccessUIState(allLeagues = allLeaguesNames)
                    else -> footballModelMapper.toErrorUIState()
                }
            }
        }
    }

    fun onSearchChanged(
        newSearchText: String
    ) {
        viewModelScope.launch {
            currentSearchText = newSearchText
            _uiState.update {
                footballModelMapper.updateUIStateBySearchText(
                    currentState = it,
                    searchText = newSearchText,
                    allLeagues = allLeaguesNames
                )
            }
        }
    }

    fun initTeamDetailState() {
        viewModelScope.launch {
            _teamDetailState.update { TeamDetailUIState.Loading }
            getTeamDetailByNameUseCase(selectedTeamName).collect { team ->
                when (team) {
                    null -> _teamDetailState.update { teamDetailModelMapper.toErrorUIState(selectedTeamName) }
                    else -> _teamDetailState.update { teamDetailModelMapper.toSuccessUIState(team) }
                }
            }
        }
    }
}