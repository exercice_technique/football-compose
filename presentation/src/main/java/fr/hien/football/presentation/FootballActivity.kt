package fr.hien.football.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import fr.hien.football.presentation.component.ErrorComponent
import fr.hien.football.presentation.component.LoaderComponent
import fr.hien.football.presentation.screen.ScreenContainer
import fr.hien.football.presentation.ui.state.FootballUIState
import fr.hien.football.presentation.ui.theme.FootballTheme

@AndroidEntryPoint
class FootballActivity : ComponentActivity() {

    private val viewModel: FootballViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Not need to re-init when change the config (rotation, theme, etc)
        if (viewModel.allLeaguesNames.isEmpty()) {
            viewModel.initUIState()
        }
        setContent {
            FootballTheme {
                when (val uiState = viewModel.uiState.collectAsStateWithLifecycle().value) {
                    FootballUIState.Loading -> LoaderComponent()
                    is FootballUIState.Error -> ErrorComponent(
                        uiModel = uiState.uiModel,
                        onButtonClick = { viewModel.initUIState() }
                    )
                    is FootballUIState.Success -> ScreenContainer(
                        viewModel = viewModel,
                        uiModel = uiState
                    )
                }
            }
        }
    }
}