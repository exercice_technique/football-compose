package fr.hien.football.presentation.screen

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import fr.hien.football.presentation.FootballViewModel
import fr.hien.football.presentation.ui.state.FootballUIState

@Composable
fun ScreenContainer(
    viewModel: FootballViewModel,
    uiModel: FootballUIState.Success,
    navController: NavHostController = rememberNavController()
) {
    NavHost(
        navController = navController,
        startDestination = ScreenType.TEAMS.name
    ) {
        composable(route = ScreenType.TEAMS.name) {
            TeamsScreen(
                viewModel = viewModel,
                uiModel = uiModel,
                navController = navController
            )
        }
        composable(route = ScreenType.TEAM_DETAIL.name) {
            TeamDetailScreen(
                viewModel = viewModel,
                navController = navController
            )
        }
    }
}