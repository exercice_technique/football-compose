package fr.hien.football.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class SearchByLeagueUIModel(
    val searchText: String,
    val leaguesNames: List<String>
)