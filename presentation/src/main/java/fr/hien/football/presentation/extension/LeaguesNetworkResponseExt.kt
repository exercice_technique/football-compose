package fr.hien.football.presentation.extension

import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus

fun NetworkResponse<Leagues>.toLeaguesNames() = when (status) {
    RequestStatus.SUCCESS -> data?.leagues.orEmpty().sortedBy {
        it.name
    }.map {
        it.name
    }
    else -> emptyList()
}
