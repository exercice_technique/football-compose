package fr.hien.football.presentation.ui.mapper

import fr.hien.football.domain.model.Team
import fr.hien.football.presentation.R
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.TeamDetailUIModel
import fr.hien.football.presentation.ui.model.TextUIModel
import fr.hien.football.presentation.ui.state.TeamDetailUIState
import javax.inject.Inject

class TeamDetailModelMapper @Inject constructor() {

    fun toErrorUIState(
        teamName: String
    ) = TeamDetailUIState.Error(
        uiModel = ErrorUIModel(
            text = TextUIModel.StringResource(R.string.error_of_get_team, arrayOf(teamName))
        )
    )

    fun toSuccessUIState(
        team: Team
    ) = TeamDetailUIState.Success(
        uiModel = TeamDetailUIModel(
            name = TextUIModel.DynamicString(team.name),
            bannerUrl = team.bannerUrl.orEmpty(),
            countryName = TextUIModel.DynamicString(team.countryName.orEmpty()),
            leagueName = TextUIModel.DynamicString(team.leagueName.orEmpty()),
            description = TextUIModel.DynamicString(team.englishDescription.orEmpty()),
        )
    )
}