package fr.hien.football.presentation.ui.mapper

import fr.hien.football.domain.usecase.GetTeamsByLeagueNameUseCase
import fr.hien.football.presentation.R
import fr.hien.football.presentation.extension.toTeamUIModels
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.TextUIModel
import fr.hien.football.presentation.ui.state.FootballUIState
import javax.inject.Inject

class FootballModelMapper @Inject constructor(
    private val getTeamsByLeagueNameUseCase: GetTeamsByLeagueNameUseCase
) {

    fun toSuccessUIState(
        allLeagues: List<String>
    ) = FootballUIState.Success(
        leagues = allLeagues,
        teams = emptyList()
    )

    fun toErrorUIState() = FootballUIState.Error(
        uiModel = ErrorUIModel(
            text = TextUIModel.StringResource(R.string.error_of_get_leagues)
        )
    )

    suspend fun updateUIStateBySearchText(
        currentState: FootballUIState,
        searchText: String,
        allLeagues: List<String>
    ): FootballUIState {
        if (currentState !is FootballUIState.Success) return currentState
        val teamsResult = getTeamsByLeagueNameUseCase(searchText)
        val filteredLeagues = allLeagues.filter {
            searchText.isBlank() || it.contains(other = searchText.lowercase(), ignoreCase = true)
        }
        return currentState.copy(
            leagues = filteredLeagues,
            teams = teamsResult.toTeamUIModels()
        )
    }
}