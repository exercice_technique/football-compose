package fr.hien.football.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class ErrorUIModel(
    val text: TextUIModel
)
