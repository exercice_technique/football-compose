package fr.hien.football.presentation.ui.model.previewprovider

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import fr.hien.football.presentation.ui.model.TeamDetailUIModel
import fr.hien.football.presentation.ui.model.TextUIModel

class TeamDetailUIModelPreviewProvider : PreviewParameterProvider<TeamDetailUIModel> {

    override val values: Sequence<TeamDetailUIModel> = sequenceOf(
        TeamDetailUIModel(
            name = TextUIModel.DynamicString("Barcelona"),
            bannerUrl = "https://www.thesportsdb.com/images/media/team/banner/h3sewn1557002132.jpg",
            countryName = TextUIModel.DynamicString("Spain"),
            leagueName = TextUIModel.DynamicString("La Liga"),
            description = TextUIModel.DynamicString("Futbol Club Barcelona, also known as Barcelona")
        ),
        TeamDetailUIModel(
            name = TextUIModel.DynamicString("PSG"),
            bannerUrl = "https://www.thesportsdb.com/images/media/team/banner/h3sewn1557002132.jpg",
            countryName = TextUIModel.DynamicString("France"),
            leagueName = TextUIModel.DynamicString("Ligue 1"),
            description = TextUIModel.DynamicString("PSG club")
        ),
    )
}