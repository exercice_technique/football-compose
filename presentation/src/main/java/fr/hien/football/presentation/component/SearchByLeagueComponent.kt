package fr.hien.football.presentation.component

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowDown
import androidx.compose.material.icons.rounded.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fr.hien.football.presentation.R
import fr.hien.football.presentation.annotation.CombinedPreviews
import fr.hien.football.presentation.ui.model.SearchByLeagueUIModel
import fr.hien.football.presentation.ui.model.TextUIModel

@Composable
fun SearchByLeagueComponent(
    uiModel: SearchByLeagueUIModel,
    onSearchChanged: (String) -> Unit
) {
    var expanded by remember {
        mutableStateOf(false)
    }
    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
            .clickable(
                onClick = { expanded = false }
            )
    ) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Row(modifier = Modifier.fillMaxWidth()) {
                TextField(
                    modifier = Modifier.fillMaxWidth(),
                    value = uiModel.searchText,
                    onValueChange = { newValue ->
                        expanded = true
                        onSearchChanged(newValue)
                    },
                    colors = TextFieldDefaults.colors(
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        cursorColor = Color.Black
                    ),
                    textStyle = TextStyle(
                        color = Color.Black,
                        fontSize = 16.sp
                    ),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Done
                    ),
                    singleLine = true,
                    trailingIcon = {
                        IconButton(onClick = { expanded = !expanded }) {
                            Icon(
                                modifier = Modifier.size(24.dp),
                                imageVector = Icons.Rounded.KeyboardArrowDown,
                                contentDescription = stringResource(id = R.string.arrow_down_description),
                                tint = Color.Black
                            )
                        }
                    },
                    leadingIcon = {
                        Icon(
                            modifier = Modifier.size(24.dp),
                            imageVector = Icons.Rounded.Search,
                            contentDescription = stringResource(id = R.string.search_icon_description),
                            tint = Color.Gray
                        )
                    },
                    placeholder = {
                        Text(stringResource(id = R.string.search_by_league))
                    }
                )
            }
            AnimatedVisibility(visible = expanded) {
                LazyColumn(
                    modifier = Modifier.heightIn(max = 250.dp)
                ) {
                    item { Spacer(modifier = Modifier.height(8.dp)) }
                    uiModel.leaguesNames.forEach {
                        item {
                            LeagueItemComponent(
                                uiModel = TextUIModel.DynamicString(it),
                                onSelect = { selectedText ->
                                    expanded = false
                                    onSearchChanged(selectedText)
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun LeagueItemComponent(
    uiModel: TextUIModel,
    onSelect: (String) -> Unit
) {
    val text = uiModel.asString()
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onSelect(text) }
    ) {
        Text(
            text = uiModel.asString(),
            fontSize = 16.sp,
            color = Color.Blue
        )
    }
}

@Composable
@CombinedPreviews
fun PreviewSearchByLeagueComponent() {
    SearchByLeagueComponent(
        uiModel = SearchByLeagueUIModel(
            leaguesNames = listOf(
                "Ligue 1",
                "La Liga",
                "Premier League"
            ),
            searchText = ""
        ),
        onSearchChanged = {}
    )
}














