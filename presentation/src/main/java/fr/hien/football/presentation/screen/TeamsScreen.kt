package fr.hien.football.presentation.screen

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.exclude
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.layout.systemBars
import androidx.compose.foundation.layout.windowInsetsPadding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import fr.hien.football.presentation.FootballViewModel
import fr.hien.football.presentation.component.SearchByLeagueComponent
import fr.hien.football.presentation.component.TeamsComponent
import fr.hien.football.presentation.ui.model.SearchByLeagueUIModel
import fr.hien.football.presentation.ui.state.FootballUIState

@Composable
fun TeamsScreen(
    viewModel: FootballViewModel,
    uiModel: FootballUIState.Success,
    navController: NavHostController
) {
    val activity = LocalContext.current as Activity
    BackHandler {
        activity.finish()
    }
    Scaffold(
        modifier = Modifier.windowInsetsPadding(WindowInsets.systemBars.exclude(WindowInsets.statusBars)),
        topBar = {
            Surface(
                modifier = Modifier.fillMaxWidth(),
                shadowElevation = 8.dp,
                color = Color.LightGray
            ) {
                SearchByLeagueComponent(
                    uiModel = SearchByLeagueUIModel(
                        searchText = viewModel.currentSearchText,
                        leaguesNames = uiModel.leagues
                    ),
                    onSearchChanged = { newText ->
                        viewModel.onSearchChanged(newText)
                    }
                )
            }
        },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
                    .fillMaxWidth()
            ) {
                TeamsComponent(
                    uiModel = uiModel.teams,
                    onTeamClick = { teamName ->
                        viewModel.selectedTeamName = teamName
                        viewModel.initTeamDetailState()
                        navController.navigate(ScreenType.TEAM_DETAIL.name)
                    }
                )
            }
        }
    )
}