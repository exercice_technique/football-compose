package fr.hien.football.presentation.screen

enum class ScreenType {
    TEAMS,
    TEAM_DETAIL
}