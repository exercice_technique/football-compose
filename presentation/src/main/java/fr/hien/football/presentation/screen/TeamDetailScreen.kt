package fr.hien.football.presentation.screen

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavHostController
import fr.hien.football.presentation.FootballViewModel
import fr.hien.football.presentation.component.ErrorComponent
import fr.hien.football.presentation.component.LoaderComponent
import fr.hien.football.presentation.component.TeamDetailComponent
import fr.hien.football.presentation.ui.state.TeamDetailUIState

@Composable
fun TeamDetailScreen(
    viewModel: FootballViewModel,
    navController: NavHostController,
) {
    BackHandler {
        navController.navigate(ScreenType.TEAMS.name)
    }
    when (val uiState = viewModel.teamDetailState.collectAsStateWithLifecycle().value) {
        TeamDetailUIState.Loading -> LoaderComponent()
        is TeamDetailUIState.Error -> ErrorComponent(
            uiModel = uiState.uiModel,
            onButtonClick = { viewModel.initTeamDetailState() }
        )
        is TeamDetailUIState.Success -> TeamDetailComponent(uiModel = uiState.uiModel)
    }
}