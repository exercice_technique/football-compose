package fr.hien.football.presentation.ui.state

import androidx.compose.runtime.Immutable
import fr.hien.football.presentation.ui.model.ErrorUIModel
import fr.hien.football.presentation.ui.model.SearchByLeagueUIModel
import fr.hien.football.presentation.ui.model.TeamUIModel

@Immutable
sealed class FootballUIState {
    @Immutable
    object Loading : FootballUIState()

    @Immutable
    data class Error(
        val uiModel: ErrorUIModel
    ) : FootballUIState()

    @Immutable
    data class Success(
        val leagues: List<String>,
        val teams: List<TeamUIModel>
    ) : FootballUIState()
}