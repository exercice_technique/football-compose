package fr.hien.football.presentation.extension

import fr.hien.football.domain.model.Teams
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.presentation.ui.model.TeamUIModel

fun NetworkResponse<Teams>.toTeamUIModels() = when (status) {
    RequestStatus.SUCCESS -> data?.teams.orEmpty().sortedByDescending { team ->
        team.name
    }.map { team ->
        TeamUIModel(
            badgeUrl = team.badgeUrl.orEmpty(),
            name = team.name
        )
    }
    else -> emptyList()
}
