package fr.hien.football.data.source.remote.model

data class TeamsRemote(
    val teams: List<TeamRemote>? = emptyList()
)