package fr.hien.football.data.source.remote

import fr.hien.football.data.source.remote.model.TeamsRemote
import javax.inject.Inject

class TeamsRemoteDataSource @Inject constructor(
    private val ws: LeaguesWS
) : ApiDataSource<TeamsRemote>() {

    suspend fun getTeamsByLeagueName(
        leagueName: String
    ) = launchRequest(ws.getRemoteTeamsByLeagueName(leagueName))
}