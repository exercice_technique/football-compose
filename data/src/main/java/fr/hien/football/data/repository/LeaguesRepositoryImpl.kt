package fr.hien.football.data.repository

import fr.hien.football.data.source.extension.toLeagues
import fr.hien.football.data.source.remote.LeaguesRemoteDataSource
import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.repository.LeaguesRepository
import javax.inject.Inject

class LeaguesRepositoryImpl @Inject constructor(
    private val leaguesRemoteDataSource: LeaguesRemoteDataSource
) : LeaguesRepository {

    override suspend fun getLeagues(): NetworkResponse<Leagues> {
        return leaguesRemoteDataSource.getLeagues().run {
            NetworkResponse(
                data = data?.toLeagues(),
                status = status,
                code = code
            )
        }
    }
}