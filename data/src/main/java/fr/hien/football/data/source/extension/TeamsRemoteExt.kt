package fr.hien.football.data.source.extension

import fr.hien.football.data.source.remote.model.TeamsRemote
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams

fun TeamsRemote.toTeams() = Teams(
    teams = teams?.map {
        Team(
            id = it.id,
            name = it.name,
            logoUrl = it.logoUrl,
            badgeUrl = it.badgeUrl,
            bannerUrl = it.bannerUrl,
            countryName = it.countryName,
            leagueName = it.leagueName,
            englishDescription = it.englishDescription
        )
    }
)