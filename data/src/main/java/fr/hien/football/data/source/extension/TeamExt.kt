package fr.hien.football.data.source.extension

import fr.hien.football.data.source.local.db.entity.TeamEntity
import fr.hien.football.domain.model.Team

fun Team.toEntity() = TeamEntity(
    id = id,
    name = name,
    logoUrl = logoUrl,
    badgeUrl = badgeUrl,
    bannerUrl = bannerUrl,
    countryName = countryName,
    leagueName = leagueName,
    englishDescription = englishDescription
)