package fr.hien.football.data.source.remote.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class TeamRemote(
    @SerializedName("idTeam") val id: String,
    @SerializedName("strTeam") val name: String,
    @SerializedName("strTeamLogo") val logoUrl: String?,
    @SerializedName("strTeamBadge") val badgeUrl: String?,
    @SerializedName("strTeamBanner") val bannerUrl: String?,
    @SerializedName("strCountry") val countryName: String?,
    @SerializedName("strLeague") val leagueName: String,
    @SerializedName("strDescriptionEN") val englishDescription: String?
)
