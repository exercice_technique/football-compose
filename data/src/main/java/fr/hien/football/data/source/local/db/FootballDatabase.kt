package fr.hien.football.data.source.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.hien.football.data.source.local.db.dao.TeamDao
import fr.hien.football.data.source.local.db.entity.TeamEntity

@Database(
    entities = [
        TeamEntity::class
    ],
    version = 1,
    exportSchema = true
)

abstract class FootballDatabase : RoomDatabase() {
    abstract val teamDao: TeamDao
}