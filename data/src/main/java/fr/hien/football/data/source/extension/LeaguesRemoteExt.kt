package fr.hien.football.data.source.extension

import fr.hien.football.data.source.remote.model.LeaguesRemote
import fr.hien.football.domain.model.League
import fr.hien.football.domain.model.Leagues

fun LeaguesRemote.toLeagues() = Leagues(
    leagues = leagues.map {
        League(
            id = it.id,
            name = it.name,
            sportName = it.sportName,
            alternateName = it.alternateName
        )
    }
)