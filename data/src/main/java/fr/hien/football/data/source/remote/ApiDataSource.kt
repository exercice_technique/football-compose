package fr.hien.football.data.source.remote

import android.util.Log
import fr.hien.football.domain.extension.toNetworkResponse
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call

abstract class ApiDataSource<TModel>(
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {

    @Suppress("UNCHECKED_CAST")
    suspend fun launchRequest(
        call: Call<TModel>
    ): NetworkResponse<TModel> {
        return try {
            withContext(dispatcher) {
                val response = call.execute()
                if (response.isSuccessful) {
                    NetworkResponse(response.body(), RequestStatus.SUCCESS, response.code())
                } else {
                    NetworkResponse(null, RequestStatus.ERR_REJECTED, response.code())
                }
            }
        } catch (exception: Exception) {
            Log.e("ApiDataSource", "Exception : $exception")
            exception.toNetworkResponse() as NetworkResponse<TModel>
        }
    }
}
