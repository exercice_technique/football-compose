package fr.hien.football.data.source.remote

import fr.hien.football.data.source.remote.model.LeaguesRemote
import fr.hien.football.domain.model.NetworkResponse
import javax.inject.Inject

class LeaguesRemoteDataSource @Inject constructor(
    private val ws: LeaguesWS
) : ApiDataSource<LeaguesRemote>() {

    suspend fun getLeagues(): NetworkResponse<LeaguesRemote> = launchRequest(ws.getRemoteLeagues())
}

