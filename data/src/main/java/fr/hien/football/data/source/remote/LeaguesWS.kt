package fr.hien.football.data.source.remote

import fr.hien.football.data.source.remote.model.LeaguesRemote
import fr.hien.football.data.source.remote.model.TeamsRemote
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface LeaguesWS {

    @GET("search_all_teams.php?")
    fun getRemoteTeamsByLeagueName(
        @Query("l") leagueName: String
    ): Call<TeamsRemote>

    @GET("all_leagues.php")
    fun getRemoteLeagues(): Call<LeaguesRemote>
}