package fr.hien.football.data.repository

import fr.hien.football.data.source.extension.toTeams
import fr.hien.football.data.source.local.TeamsLocalDataSource
import fr.hien.football.data.source.remote.TeamsRemoteDataSource
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import fr.hien.football.domain.repository.TeamsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TeamsRepositoryImpl @Inject constructor(
    private val teamsLocalDataSource: TeamsLocalDataSource,
    private val teamsRemoteDataSource: TeamsRemoteDataSource
) : TeamsRepository {

    override suspend fun getTeamByName(name: String): Flow<Team?> {
        return teamsLocalDataSource.findTeamByName(name)
    }

    override suspend fun saveTeams(teams: Teams) {
        teamsLocalDataSource.saveTeams(teams)
    }

    override suspend fun getTeamsByLeagueName(leagueName: String): NetworkResponse<Teams> {
        return teamsRemoteDataSource.getTeamsByLeagueName(leagueName).run {
            NetworkResponse(
                data = data?.toTeams(),
                status = status,
                code = code
            )
        }
    }
}