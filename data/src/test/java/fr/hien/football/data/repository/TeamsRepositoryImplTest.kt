package fr.hien.football.data.repository

import fr.hien.football.data.source.local.TeamsLocalDataSource
import fr.hien.football.data.source.remote.TeamsRemoteDataSource
import fr.hien.football.data.source.remote.model.TeamRemote
import fr.hien.football.data.source.remote.model.TeamsRemote
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
@ExperimentalCoroutinesApi
internal class TeamsRepositoryImplTest {

    @InjectMockKs
    private lateinit var repository: TeamsRepositoryImpl

    @MockK
    private lateinit var teamsRemoteDataSource: TeamsRemoteDataSource

    @MockK
    private lateinit var teamsLocalDataSource: TeamsLocalDataSource

    @Test
    fun `WHEN getTeamsByLeagueName of TeamsRemoteDataSource gives the teams remote THEN getTeamsByLeagueName returns these matching teams`() = runTest {
        // GIVEN
        val teamsRemoteNetworkResponse = NetworkResponse(
            data = teamsRemote
        )
        val leagueName = "Lingue 1"
        coEvery { (teamsRemoteDataSource.getTeamsByLeagueName(leagueName)) } returns (teamsRemoteNetworkResponse)

        // WHEN
        val actual = repository.getTeamsByLeagueName(leagueName)

        // THEN
        coVerify { teamsRemoteDataSource.getTeamsByLeagueName(leagueName) }
        val expected = NetworkResponse(
            data = teams
        )
        assertEquals(expected, actual)
    }

    @Test
    fun `WHEN findTeamByName of TeamsLocalDataSource gives a result THEN getTeamByName returns this result`() = runTest {
        // GIVEN
        coEvery { teamsLocalDataSource.findTeamByName(TEAM_NAME) } returns teamFlow

        // WHEN
        val actual = repository.getTeamByName(TEAM_NAME)

        // THEN
        coVerify { teamsLocalDataSource.findTeamByName(TEAM_NAME) }
        assertEquals(teamFlow, actual)
    }

    @Test
    fun `WHEN saveTeams of TeamsLocalDataSource THEN saveTeams calls this method`() = runTest {
        // GIVEN
        coEvery { teamsLocalDataSource.saveTeams(teams) } returns Unit

        // WHEN
        repository.saveTeams(teams)

        // THEN
        coVerify { teamsLocalDataSource.saveTeams(teams) }
    }

    companion object {

        private const val TEAM_NAME = "PSG"
        private val team = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val teamFlow = flowOf(team)
        private val teams = Teams(
            teams = listOf(team)
        )
        private val teamRemote = TeamRemote(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val teamsRemote = TeamsRemote(
            teams = listOf(teamRemote)
        )
    }
}