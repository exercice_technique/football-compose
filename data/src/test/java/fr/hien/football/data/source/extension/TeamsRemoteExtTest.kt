package fr.hien.football.data.source.extension

import fr.hien.football.data.source.remote.model.TeamRemote
import fr.hien.football.data.source.remote.model.TeamsRemote
import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class TeamsRemoteExtTest {

    @ParameterizedTest(name = "WHEN TeamsRemote: {0} THEN toTeams returns Teams: {1}")
    @MethodSource("provideTestData")
    fun toLeaguesNames(
        teamsRemote: TeamsRemote,
        expected: Teams
    ) {
        assertEquals(expected, teamsRemote.toTeams())
    }

    companion object {

        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val lilleTeam = psgTeam.copy(
            name = "Lille",
            badgeUrl = "lille-url"
        )
        private val psgTeamRemote = TeamRemote(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val lilleTeamRemote = psgTeamRemote.copy(
            name = "Lille",
            badgeUrl = "lille-url"
        )

        @JvmStatic
        private fun provideTestData(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    TeamsRemote(teams = null),
                    Teams(teams = null)
                ),
                Arguments.of(
                    TeamsRemote(),
                    Teams()
                ),
                Arguments.of(
                    TeamsRemote(
                        teams = listOf(psgTeamRemote, lilleTeamRemote)
                    ),
                    Teams(
                        teams = listOf(psgTeam, lilleTeam)
                    )
                )
            )
        }
    }
}