package fr.hien.football.data.source.extension

import fr.hien.football.data.source.remote.model.LeagueRemote
import fr.hien.football.data.source.remote.model.LeaguesRemote
import fr.hien.football.domain.model.League
import fr.hien.football.domain.model.Leagues
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LeaguesRemoteExtTest {

    @Test
    fun toLeagues() {
        assertEquals(leagues, leaguesRemote.toLeagues())
    }

    companion object {

        private val premierLeague = League(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val premierLeagueRemote = LeagueRemote(
            id = 4328,
            name = "English Premier League",
            alternateName = "",
            sportName = ""
        )
        private val leagues = Leagues(
            leagues = listOf(premierLeague)
        )
        private val leaguesRemote = LeaguesRemote(
            leagues = listOf(premierLeagueRemote)
        )
    }
}