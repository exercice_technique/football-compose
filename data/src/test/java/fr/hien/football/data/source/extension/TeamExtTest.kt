package fr.hien.football.data.source.extension

import fr.hien.football.data.source.local.db.entity.TeamEntity
import fr.hien.football.domain.model.Team
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TeamExtTest {

    @Test
    fun toEntity() {
        assertEquals(psgTeamEntity, psgTeam.toEntity())
    }

    companion object {

        private val psgTeam = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val psgTeamEntity = TeamEntity(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
    }
}