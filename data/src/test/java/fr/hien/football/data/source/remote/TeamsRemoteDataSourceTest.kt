package fr.hien.football.data.source.remote

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import fr.hien.football.data.source.remote.model.TeamsRemote
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension
import retrofit2.Call
import retrofit2.Response
import java.net.HttpURLConnection

@ExtendWith(MockitoExtension::class)
@ExperimentalCoroutinesApi
internal class TeamsRemoteDataSourceTest {

    @InjectMocks
    private lateinit var dataSource: TeamsRemoteDataSource

    @Mock
    private lateinit var ws: LeaguesWS

    @Mock
    private lateinit var call: Call<TeamsRemote>

    @Test
    fun `WHEN LeaguesWS gives a result THEN getTeamsByLeagueName returns this result`() = runTest {
        // GIVEN
        val teams = TeamsRemote(teams = emptyList())
        val leagueName = "Ligue 1"
        given(ws.getRemoteTeamsByLeagueName(leagueName)).willReturn(call)
        given(call.execute()).willReturn(Response.success(teams))

        // WHEN
        val actual = dataSource.getTeamsByLeagueName(leagueName)

        // THEN
        then(ws).should().getRemoteTeamsByLeagueName(leagueName)
        then(call).should().execute()
        val expected = NetworkResponse(
            data = teams,
            status = RequestStatus.SUCCESS,
            code = HttpURLConnection.HTTP_OK
        )
        assertEquals(expected, actual)
    }
}