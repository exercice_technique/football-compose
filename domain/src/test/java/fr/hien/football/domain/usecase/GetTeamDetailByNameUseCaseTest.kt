package fr.hien.football.domain.usecase

import fr.hien.football.domain.model.Team
import fr.hien.football.domain.repository.TeamsRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class GetTeamDetailByNameUseCaseTest {

    @InjectMockKs
    private lateinit var useCase: GetTeamDetailByNameUseCase

    @MockK
    private lateinit var teamsRepository: TeamsRepository

    @Test
    operator fun invoke() = runTest {
        // GIVEN
        coEvery { teamsRepository.getTeamByName(TEAM_NAME) } returns teamFlow

        // WHEN
        val actual = useCase(TEAM_NAME)

        // THEN
        coVerify { teamsRepository.getTeamByName(TEAM_NAME) }
        assertEquals(teamFlow, actual)
    }

    companion object {

        private const val TEAM_NAME = "PSG"
        private val team = Team(
            id = "1",
            name = "PSG",
            logoUrl = "",
            badgeUrl = "psg-url",
            bannerUrl = "",
            countryName = "",
            leagueName = "",
            englishDescription = ""
        )
        private val teamFlow = flowOf(team)
    }
}