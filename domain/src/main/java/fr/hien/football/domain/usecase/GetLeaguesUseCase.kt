package fr.hien.football.domain.usecase

import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.repository.LeaguesRepository
import javax.inject.Inject

class GetLeaguesUseCase @Inject constructor(
    private val leaguesRepository: LeaguesRepository
) {

    suspend operator fun invoke(): NetworkResponse<Leagues> {
        return leaguesRepository.getLeagues()
    }
}