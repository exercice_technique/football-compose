package fr.hien.football.domain.model

data class Team(
    val id: String,
    val name: String,
    val logoUrl: String?,
    val badgeUrl: String?,
    val bannerUrl: String?,
    val countryName: String?,
    val leagueName: String,
    val englishDescription: String?
)
