package fr.hien.football.domain.extension

import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import java.io.IOException

fun Exception.toNetworkResponse() = when (this) {
    is IOException -> NetworkResponse<Any>(null, RequestStatus.ERR_NETWORK)
    else -> NetworkResponse<Any>(null, RequestStatus.ERR_NOT_REACHED)
}