package fr.hien.football.domain.model

data class Leagues(
    val leagues: List<League> = emptyList()
)
