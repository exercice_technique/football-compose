package fr.hien.football.domain.usecase

import fr.hien.football.domain.repository.TeamsRepository
import javax.inject.Inject

class GetTeamDetailByNameUseCase @Inject constructor(
    private val teamsRepository: TeamsRepository
) {

    suspend operator fun invoke(
        name: String
    ) = teamsRepository.getTeamByName(name = name)
}