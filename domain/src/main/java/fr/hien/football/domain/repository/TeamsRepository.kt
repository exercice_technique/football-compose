package fr.hien.football.domain.repository

import fr.hien.football.domain.model.Team
import fr.hien.football.domain.model.Teams
import fr.hien.football.domain.model.NetworkResponse
import kotlinx.coroutines.flow.Flow

interface TeamsRepository {

    suspend fun getTeamByName(name: String): Flow<Team?>

    suspend fun saveTeams(teams: Teams)

    suspend fun getTeamsByLeagueName(leagueName: String): NetworkResponse<Teams>
}