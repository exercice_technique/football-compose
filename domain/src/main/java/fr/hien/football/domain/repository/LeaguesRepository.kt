package fr.hien.football.domain.repository

import fr.hien.football.domain.model.Leagues
import fr.hien.football.domain.model.NetworkResponse

interface LeaguesRepository {

    suspend fun getLeagues(): NetworkResponse<Leagues>
}