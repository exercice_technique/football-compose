package fr.hien.football.domain.model

data class Teams(
    val teams: List<Team>? = emptyList()
)