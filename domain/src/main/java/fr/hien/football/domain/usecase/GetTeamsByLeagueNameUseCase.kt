package fr.hien.football.domain.usecase

import fr.hien.football.domain.model.NetworkResponse
import fr.hien.football.domain.model.RequestStatus
import fr.hien.football.domain.model.Teams
import fr.hien.football.domain.repository.TeamsRepository
import javax.inject.Inject

class GetTeamsByLeagueNameUseCase @Inject constructor(
    private val teamsRepository: TeamsRepository
) {

    suspend operator fun invoke(
        leagueName: String
    ): NetworkResponse<Teams> {
        val result = teamsRepository.getTeamsByLeagueName(leagueName)
        if (result.status == RequestStatus.SUCCESS) {
            result.data?.let {
                teamsRepository.saveTeams(it)
            }
        }
        return result
    }
}